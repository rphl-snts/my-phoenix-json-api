#!/bin/sh

# pinging the app and waiting until it's up would probably be nicer
sleep 3 

echo "Running migrations"
release_ctl eval --mfa "MyApp.ReleaseTasks.migrate/1" --argv -- "$@"
