FROM elixir:latest

RUN apt-get update \
 && apt-get install -y apt-utils build-essential inotify-tools postgresql-client

ENV APP_HOST=${APP_HOST}
ENV APP_PORT=${APP_PORT}
ENV PGUSER=${PGUSER}
ENV PGPASSWORD=${PGPASSWORD}
ENV PGDATABASE=${PGDATABASE}
ENV PGDATA=${PGDATA}
ENV PGPORT=${PGPORT}
ENV PGHOST=${PGHOST}
ENV DATABASE_URL=${DATABASE_URL}
ENV SECRET_KEY_BASE=${SECRET_KEY_BASE}

RUN mkdir /app
COPY . /app
WORKDIR /app

ENV MIX_HOME=/opt/mix
RUN mix local.rebar --force
RUN mix local.hex --force
RUN mix deps.get --only prod
RUN MIX_ENV=prod mix compile

RUN chmod +x /app/entrypoint.sh
CMD ["/app/entrypoint.sh"]
