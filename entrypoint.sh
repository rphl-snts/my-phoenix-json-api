#!/bin/bash
# Docker entrypoint script.

# Wait until Postgres is ready
echo $PGUSER
echo $PGPASSWORD
echo $PGDATABASE
echo $PGPORT
echo $PGHOST
echo $DATABASE_URL
echo $SECRET_KEY_BASE

# while !pg_isready -q -h $PGHOST -p $PGPORT -U $PGUSER
# do
#   echo "$(date) - waiting for database to start"
#   sleep 2
# done

# Create, migrate, and seed database if it doesn't exist.
if [[ -z `psql -Atqc "\\list $PGDATABASE"` ]]; then
  echo "Database $PGDATABASE does not exist. Creating..."
  mix ecto.setup
  echo "Database $PGDATABASE created."
fi

exec mix phx.server
