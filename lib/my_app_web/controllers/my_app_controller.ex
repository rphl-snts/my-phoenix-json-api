defmodule MyAppWeb.Controllers.MyAppController do
  use MyAppWeb, :controller

  def index(conn, _params) do
    conn
    |> json(%{hello: "world"})
  end
end
