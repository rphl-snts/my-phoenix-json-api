defmodule MyAppWeb.Router do
  use MyAppWeb, :router
  alias Controllers.MyAppController

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", MyAppWeb do
    pipe_through :api
  end

  scope "/", MyAppWeb do
    get "/", MyAppController, :index
  end
end
